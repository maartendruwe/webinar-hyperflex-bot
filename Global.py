#File with settings, variables, ...

ACCESS_TOKEN = "Put your bot access token here" #access token for bot
SOCKET_IO_PORT = 8080
requestList = {}

### WEBHOOK INFO ###
webhookTargetURL = "myboturl.com/spark" #replace with the IP address of your bot
webhookName = "Webhook for HyperFlex bot"
webhookID = "" #keep empty

### WEBSOCKET ###
activeSockets = {} #initialize list with ids of active websockets
