Our code consists of 2 main parts: the spark bot and the hyperflex broker.
The only code needed for the hyperflex broker is in the hx_broker directory.
The other files are needed for the spark bot.

The hyperflex broker should be deployed on a server that can connect to your
hyperflex cluster, but that can also has web access (outgoing http requests).

The spark bot should be hosted on a web server (ingoing and outgoing http
requests). That could be on-premise or in the cloud.
