
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from socketIO_client import SocketIO
import logging
from hurry.filesize import size

# Disable SSL warnings
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Config: Replace with your own data
hxClusters = [
    {
        'url': 'https://10.9.14.46',
        'username': 'administrator@vsphere.local',
        'password': 'UltraSecretPassword'
    },
    {
        'url': 'https://10.9.14.121',
        'username': 'administrator@vsphere.local',
        'password': 'MegaSuperSecretPassword'
    },
]
botUrl = 'http://simple-it.herokuapp.com' #Replace by your own URL


def auth(cluster):
    payload = {
        'username': cluster['username'],
        'password': cluster['password'],
        'client_id': 'HxGuiClient',
        'client_secret': 'Sunnyvale',
        'redirect_uri': 'http://localhost'
    }
    headers = {'Connection': 'close'}
    r = requests.post(cluster['url'] + '/aaa/v1/auth?grant_type=password', json=payload,
                      verify=False, headers=headers)
    authData = r.json()
    return authData


def get(url, uri, headers):
    return requests.get(url + uri, verify=False, headers=headers).json()


def getClusters(message):
    return hxClusters


def on_status(*args):
    print('STATUS TRIGGERED ', args)
    outputs = get_status(args[0])
    finalOutput = '## HyperFlex Details:' + "\n"
    for output in outputs:
        finalOutput += output
    payload = args[0]
    payload['status'] = finalOutput
    r = requests.post(botUrl + '/status', json=payload, verify=False)

def get_status(message):
    clusters = getClusters(message)
    outputs = []
    for cluster in clusters:
        output = ''
        authData = auth(cluster)
        url = cluster['url']
        if('token_type' in authData and 'access_token' in authData):
            headers = {'Authorization': authData['token_type'] + ' ' +
                       authData['access_token']}

            alarms = get(url, '/rest/virtplatform/alarms', headers)
            cluster = get(url, '/rest/virtplatform/cluster', headers)
            about = get(url, '/rest/about', headers)
            summary = get(url, '/rest/summary', headers)
            appliances = get(url, '/rest/appliances', headers)
            headers['Connection'] = 'close'
            virtplatform = get(url,
                               '/rest/virtplatform/vms?entityType=VIRTCLUSTER',
                               headers)

            outputAlarms = ''
            for alarm in alarms:
                outputAlarms += '* {} : {}, Status: {}, Ack: {}'.format(
                    alarm['entityName'],
                    alarm['description'],
                    alarm['status'],
                    alarm['acknowledged']
                ) + "\n"

            output += '''
### {}
* Version: {}
* Operational Status: **{}**
* Resiliency Health: **{}**
* Uptime: {}
* Capacity: **{}B** used of {}B (**{}**%)
* Storage Optimization: **{}%** - Compression {}% | Deduplication: {}%
* Nodes: {} x {}

#### Alarms:
{}


'''.format(
                cluster['name'],
                about['displayVersion'],
                summary['state'].title(),
                summary['resiliencyInfo']['state'].title(),
                summary['uptime'],
                size(summary['usedCapacity']),
                size(summary['totalCapacity']),
                round(summary['usedCapacity']/summary['totalCapacity']*100, 1),
                round(float(summary['totalSavings']), 1),
                round(float(summary['compressionSavings']), 1),
                round(float(summary['deduplicationSavings']), 1),
                cluster['numHosts'],
                about['modelNumber'],
                outputAlarms
            )
            outputs.append(output)

    return outputs


def createDatastore(cluster, name, size):
    authData = auth(cluster)
    url = cluster['url']
    if('token_type' in authData and 'access_token' in authData):
        headers = {'Authorization': authData['token_type'] + ' ' +
                   authData['access_token'], 'Connection': 'close'}
        payload = {
            'name': name,
            'size': size,
        }
        datastore = requests.post(cluster['url'] + '/rest/datastores', json=payload,
                        verify=False, headers=headers).json()
        return datastore


def on_volume(*args):
    print('VOLUME TRIGGERED ', args)
    clusters = getClusters(args[0])
    cluster = clusters[0]
    info = createDatastore(cluster, 'Demo_DataStore', 1099511627776)
    payload = args[0]
    if('virtDatastore' in info and 'url' in info['virtDatastore'] and 'entityRef' in info['virtDatastore']):
        payload['status'] = "Volume {} ({}) created".format(info['virtDatastore']['entityRef']['name'], info['virtDatastore']['url'])
    else:
        payload['status'] = "Error while creating Volume: {}".format(info['message'])
    r = requests.post(botUrl + '/status', json=payload, verify=False)


socketIO = SocketIO(botUrl)
socketIO.on('status', on_status)
socketIO.on('createVolume', on_volume)
socketIO.on('deleteVolume', on_delete_volume)
socketIO.wait()
