# Class with static methods for message handling 
from socketiohandler import SocketIOHandler
from sparkrequest import SparkRequest
import Global
import uuid
import json

class MessageHandler():

    '''Returns the message to send.'''
    @staticmethod
    def processMessage(sparkMessage, api, flasksocketio):
        text = sparkMessage.text
        senderId = sparkMessage.personId
        senderEmail = sparkMessage.personEmail
        roomId = sparkMessage.roomId
        botId = api.people.me().id
        if senderId == botId: #own message, don't respond!
            return
        requestId = str(uuid.uuid4())
        print(requestId)
        new_request = SparkRequest(requestId, senderId, roomId)
        Global.requestList[requestId] = new_request #add new_request object to dictionary of existing requests
        if 'status' in text:
            #Request HyperFlex status
            #Use socketio to request this status update!
            print('Status detected')
            payload = {'requestId':requestId}
            payload = json.loads(json.dumps(payload)) #convert to json 
            SocketIOHandler.emitMessage('status', flasksocketio, payload=payload)
            api.messages.create(roomId=sparkMessage.roomId, text="Status detected")
        elif 'volume' in text:
            print('Volume detected')
            payload = {'requestId':requestId}
            payload = json.loads(json.dumps(payload)) #convert to json 
            SocketIOHandler.emitMessage('createVolume', flasksocketio, payload=payload)
            api.messages.create(roomId=sparkMessage.roomId, text="Volume detected")

    @staticmethod
    def postStatus(jsondata, api):
        requestId = jsondata['requestId'] 
        existing_request = Global.requestList[requestId] #retrieve existing request
        message = jsondata['status']
        api.messages.create(roomId=existing_request.spaceId, text=message, markdown=message)
        

