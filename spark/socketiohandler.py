class SocketIOHandler():

    '''Returns the message to send.'''
    @staticmethod
    def emitMessage(messageType, flasksocketio, payload=None):
        if payload == None:
            flasksocketio.emit(messageType)
        else:
            flasksocketio.emit(messageType, payload)
