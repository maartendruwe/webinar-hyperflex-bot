import sys, os, json, logging, flask, random
from flask_socketio import SocketIO, emit, send, disconnect
from logging.config import dictConfig
sys.path.insert(0, os.path.join(os.path.dirname(__file__),'spark')) #set path for 'import' statements
from flask import request
from ciscosparkapi import CiscoSparkAPI #Spark API Python wrapper
import Global
from messagehandler import MessageHandler

'''
Main application.
'''
api = CiscoSparkAPI(access_token=Global.ACCESS_TOKEN)
app = flask.Flask(__name__)
flasksocketio = SocketIO(app)


def createWebhook():
    #This method is executed at the start of the script and will create a new webhook after 
    #removing all existing webhooks. This prevents having a bot with multiple existing webhooks
    #and then being notified multiple times per event. Also, we remember the webhookId of the 
    #created webhook and will only accept notifications from this webhook
    print('Creating webhook')
    existing_webhooks = api.webhooks.list()
    for existing_webhook in existing_webhooks:
        api.webhooks.delete(existing_webhook.id)
    api.webhooks.create(Global.webhookName, Global.webhookTargetURL, "messages", "created", filter=None, secret=None)
    existing_webhooks = api.webhooks.list()
    ii = 0
    for new_webhook in existing_webhooks: # remember id of new webhook
        if ii == 0: #newest webhook
            Global.webhookID = new_webhook.id
        ii = ii + 1
    print Global.webhookID


#**********
#DEFINE API CALLS!!
#**********


'''
Get notified whenever a message is posted in the Spark Room.
'''
@app.route('/spark', methods=['GET','POST'])
def sparkNotification():
    print("Spark notification received")
    if flask.request.method == 'POST':
        data=json.loads(flask.request.data.decode('utf-8')) #Get all the JSON that was sent in request
        #print(data)
        if data['id'] != Global.webhookID: #message from another webhook, let's ignore
            return ''
        sender = data['data']['personId']
        messageID = data['data']['id']
        #print(messageID)
        message = api.messages.get(messageID)
        print(message.text)
        MessageHandler.processMessage(message, api, flasksocketio)
    return ''

#To receive updates from the HyperFlex Broker
@app.route('/status', methods=['POST'])
def hyperflexStatusUpdate():
    print("HyperFlex Status Update received")
    if flask.request.method == 'POST':
        data=json.loads(flask.request.data.decode('utf-8')) #Get all the JSON that was sent in request
        MessageHandler.postStatus(data, api)
    return 'OK' #do not return None! Flask clearly doesn't like that




@app.after_request #automatically triggered after POST or GET request is handled
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  return response

@flasksocketio.on('connect') #just for logging right now, triggered on socket connection
def uiConnect(): #emit() does not work within .on('connect')
    print("Client connected to websocket.")
    

@flasksocketio.on('disconnect') #just for logging right now, triggered on socket disconnect
def uiDisconnect():
    print("Client disconnected of websocket.")



if __name__ == '__main__': #will be executed automatically

    print("init")
    createWebhook()
    app.debug = True
    flasksocketio.run(app, host='0.0.0.0', debug=False, port=int(Global.SOCKET_IO_PORT)) #initiate socket
